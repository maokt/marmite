#include <glib.h>
#include <glib-object.h>
#include <locale.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <vte/vte.h>

#include "solarized_palette.h"
#include "config.h"

typedef struct Marmite {
    GtkWindow *win;
    VteTerminal *vte;
} Marmite;

VteTerminal *marmite_vte(MarmiteConfig *cfg, Marmite *m);

int main(int argc, char *argv[]) {
    Marmite my;

    setlocale(LC_ALL, "");

    // no need for gtk_init because we use gtk_get_option_group
    MarmiteConfig *cfg = marmite_config(argc, argv, gtk_get_option_group(TRUE));
    if (!cfg) return 64;

    my.win = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
    gtk_window_set_title(my.win, cfg->title);
    g_signal_connect(G_OBJECT(my.win), "destroy", G_CALLBACK(gtk_main_quit), NULL);

    my.vte = marmite_vte(cfg, &my);
    if (!my.vte) return 70;

    gtk_container_add(GTK_CONTAINER(my.win), GTK_WIDGET(my.vte));
    gtk_widget_show_all(GTK_WIDGET(my.win));
    gtk_main();

    return 0;
}

static void got_child_exited(VteTerminal *vte, gint status, Marmite *m) {
    gtk_window_close(m->win);
}

static void got_title_changed(VteTerminal *vte, Marmite *m) {
    gtk_window_set_title(m->win, vte_terminal_get_window_title(vte));
}

static void check_spawn(VteTerminal *vte, GPid pid, GError *err, void *data) {
    Marmite *m = data;
    if (err) {
        g_warning("failed: %s", err->message);
        gtk_window_close(m->win);
    }
}

static void scale_font(VteTerminal *vte, gdouble *increment) {
    gdouble scale = vte_terminal_get_font_scale(vte);
    scale += *increment;
    if (scale >= 0.25 && scale <= 4.0)
        vte_terminal_set_font_scale(vte, scale);
}

VteTerminal *marmite_vte(MarmiteConfig *cfg, Marmite *m) {
    VteTerminal *vte = VTE_TERMINAL(vte_terminal_new());

    PangoFontDescription *font = pango_font_description_from_string(cfg->font);
    vte_terminal_set_font(vte, font);
    pango_font_description_free(font);

    vte_terminal_set_scrollback_lines(vte, cfg->scrollback);
    vte_terminal_set_mouse_autohide(vte, TRUE);
    vte_terminal_set_colors(vte,
            &solarized_palette[solarized_mode[cfg->colour_mode].fg], &solarized_palette[solarized_mode[cfg->colour_mode].bg],
            solarized_palette, solarized_palette_size);

    if (m) {
        g_signal_connect(G_OBJECT(vte), "child-exited", G_CALLBACK(got_child_exited), m);
        g_signal_connect(G_OBJECT(vte), "window-title-changed", G_CALLBACK(got_title_changed), m);
    }

    static const gdouble increase = 0.25, decrease = -0.25;
    g_signal_connect(G_OBJECT(vte), "increase-font-size", G_CALLBACK(scale_font), (gpointer)&increase);
    g_signal_connect(G_OBJECT(vte), "decrease-font-size", G_CALLBACK(scale_font), (gpointer)&decrease);

    vte_terminal_spawn_async(vte, VTE_PTY_DEFAULT, NULL, cfg->command, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, -1, NULL,
            check_spawn, (void*)m);

    return vte;
}

